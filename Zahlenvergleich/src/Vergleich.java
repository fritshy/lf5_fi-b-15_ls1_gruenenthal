import java.util.Scanner;

public class Vergleich {

	public static void main(String[] args) {
		int zahl1 = 0;
		int zahl2= 0;
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben Sie eine Zahl ein!");
		zahl1 = tastatur.nextInt();
		System.out.println("Geben Sie eine zweite Zahl ein!");
		zahl2 = tastatur.nextInt();
		
		if (zahl1 == zahl2) {
			System.out.println("Beide Zahlen sind gleich gro�.");
		}
		else if (zahl1 < zahl2) {
			System.out.println(zahl1 + " ist kleiner als " + zahl2 + ".");
		}
		else if (zahl1 >= zahl2) {
			System.out.println(zahl1 + " ist gr��er als " + zahl2 + ".");
		} //gr��er oder gleich ist an dieser Stelle eigentlich sinnlos, da bei gleicher Gr��e
			//ohnehin die erste if-Bedingung gilt und der entsprechende Teil ausgef�hrt wird
		else {
			System.out.println( "Da ist irgendwas schief gelaufen!");
		}	//Die Else- Verzweigung erscheint mir auch sinnfrei, da die Zahlen nur gleich oder unterschiedlich gro� sein k�nnen
	
		System.out.println("Aufhabe5:BMI");
		double gewicht = 0;
		double groe�e= 0;
		
		
		System.out.println( "Bitte geben Sie Ihr Gewicht in kg ein!");
		gewicht = tastatur.nextDouble();
		System.out.println( "Bitte geben Sie Ihre Groe�e in cm(!) ein!");
		groe�e = tastatur.nextDouble();
		System.out.println( "Bitte Geschlecht angeben [m/w]");
		char geschlecht = tastatur.next().charAt(0);
		groe�e = groe�e/100;
		double bmi = gewicht/(groe�e*groe�e);
		
		if (geschlecht == 'm') {
			
	
			if (bmi<20) {
				System.out.println( "Sie haben Untergewicht!");
			}
			else if (bmi<= 25) {
			System.out.println( "Sie sind normalgewichtig.");
			}
			else {
			System.out.println( "M�glicherweise sollten Sie sich bei Ihrem Arzt �ber M�glichkeiten, Gewicht zu verlieren informieren." );
			}
		}
		else {
			if (bmi<19) {
				System.out.println( "Sie haben Untergewicht!");
			}
			else if (bmi<=24) {
				System.out.println( "Sie sind normalgewichtig.");
				}
			else {
				System.out.println( "M�glicherweise sollten Sie sich bei Ihrem Arzt �ber M�glichkeiten, Gewicht zu verlieren informieren." );
				}
		}
			
	}
	
	

}
