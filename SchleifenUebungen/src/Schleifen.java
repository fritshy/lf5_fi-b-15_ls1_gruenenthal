import java.util.Scanner;
public class Schleifen {

	public static void main(String[] args) {
		//Aufgabe 1,ABB Schleifen1
		System.out.println("ABB1, Aufgabe 1: Z�hlen\n a) aufw�rts\n");
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bis zu welcher Zahl soll gez�hlt werden?");
		int zahl = tastatur.nextInt();
		for(int i = 1;i<= zahl; i++ ) {
			System.out.println(i);
		}
		
		System.out.println("\n b) ...und es geht abw�rts...\n");
		System.out.println("Von welchem Startwert soll heruntergez�hlt werden?");
		int zahl2 = tastatur.nextInt();
		for(int i = zahl2 ; i>0 ; i-- ) {
			System.out.println(i);
		}
		
		//Aufgabe2,ABB Schleifen1
		System.out.println("\n Aufgabe2:Summen");
		System.out.println("a)***Ausgabe der Summen der aufeinander folgenden Zahlen***");
		System.out.println("Bitte geben Sie ein, bis zu welcher Zahl addiert werden soll.");
		int zahln = tastatur.nextInt();
		int k = 1;
		int summen = 0;
		while(k<=zahln ) {
			summen+= k;
			k++;
			}
		System.out.println(summen);
		
		System.out.println("b)***Ausgabe der Summe der Vielfachen von 2***");
		System.out.println("Bitte geben Sie ein, bis zu welchem Vielfachen addiert werden soll.");
		int zahl3 = tastatur.nextInt();
		
		int i = 1;
		int summe = 0;
		while(i<=zahl3 ) {
			summe+= i*2;
			i++;
			}
		System.out.println(summe);
		
		System.out.println("c)***Ausgabe der Summe der Nachfolger der Vielfachen von 2***");
		System.out.println("Bitte geben Sie ein, bis zu welchem Vielfachen addiert werden soll.");
		int zahl4 = tastatur.nextInt();
		
		int j = 0;
		int summe2 = 0;
		while(j<=zahl4 ) {
			summe2+= j*2+1;
			j++;
		}
		System.out.println(summe2);
		
		//Aufgabe4,ABB Schleifen1
		System.out.println("\n Aufgabe4:Folgen");
		System.out.println("***a)\n");
		int wert = 99+3;
		do {
			wert -= 3;
			System.out.println(wert);
		} while(wert>9);
		System.out.println("\n***b)\n");
		int zahl5=0;
		int zaehler = 0;
		do {
			zahl5= zahl5+1+2*zaehler ;
			zaehler++;
			System.out.println(zahl5);
		}while(zahl5<400);
		
		System.out.println("\n***c)\n");	
		int zahl6=2-4;
		do {
			zahl6+= 4 ;
			
			System.out.println(zahl6);
		}while(zahl6<102);
		
		System.out.println("\n***d)\n");
		int zaehler2 = 1;
		int zahl7 = 0;
		do {
			zahl7=zaehler2*2 ;
			zahl7*=zahl6;
			zaehler2++;
			System.out.println(zahl7);
		}while(zaehler2<=16);
		
		System.out.println("\n***e)\n");
		int zahl8 = 1;
		do {
			zahl8*=2 ;
			
			System.out.println(zahl8);
		}while(zahl8<=16384);
		
	
	//Aufgabe 4, Blatt Schleifen2
	System.out.println("\n Aufgabe4:Temperatur");
	System.out.println("Bitte den Startwert in Celsius eingeben!");
	double temperaturmin = tastatur.nextDouble();
	System.out.println("Bitte den Endwert in Celsius eingeben!");
	double temperaturmax = tastatur.nextDouble();
	System.out.println("Bitte die Schrittweite eingeben!");
	double schrittweite = tastatur.nextDouble();
	
	double tempFahrenheit = 0.0;
	for(double cel = temperaturmin;cel<= temperaturmax; cel+= schrittweite ) {
		tempFahrenheit = cel*1.8+32;
		
				System.out.println("Die Temperatur von "+ cel + "�C entspricht einer Temperatur von "+ tempFahrenheit + "�F");
		}
	}	
}

	
