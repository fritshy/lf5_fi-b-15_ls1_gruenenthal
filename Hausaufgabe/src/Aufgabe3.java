
public class Aufgabe3 {

	public static void main(String[] args) {
	String fa = "Fahrenheit";
	String cel = "Celsius";
	char pipe = '|';
	int fa1 = -20, fa2 = -10, fa3= +0, fa4 = 20, fa5 = 30;
	float cel1 = -28.8889f, cel2 = -23.3333f, cel3 = -17.7778f,
			cel4 = -6.6667f, cel5 = -1.1111f;
	
	String s= "----------------------";
	
	System.out.println("\nAufgabe3-Hausaufgabe\n");
	System.out.printf("\n%-11s"+pipe+"%10s\n",fa,cel);
	System.out.printf("%22s\n", s );
	System.out.printf("%-11d"+pipe+"%10.2f\n",fa1,cel1);
	System.out.printf("%-11d"+pipe+"%10.2f\n",fa2,cel2);	
	System.out.printf("%+-11d"+pipe+"%10.2f\n",fa3,cel3);
	System.out.printf("%+-11d"+pipe+"%10.2f\n",fa4,cel4);
	System.out.printf("%+-11d"+pipe+"%10.2f\n",fa5,cel5);
	
	}

}
