
public class Methoden4 {

	public static void main(String[] args) {
	double seiteA = 3.0;
	double seiteB = 4.0;
	double seiteC = 5.5;
	double hoehe = 7.5;
		System.out.println(wuerfel(seiteA));	
		System.out.println(quader(seiteA,seiteB,seiteC));
		System.out.println(pyramide(seiteA,hoehe));
	}

	//Definition der Methoden
	public static double wuerfel(double a) {
	
		return a*a*a;
	}
	
	public static double quader(double a,double b, double c) {
		return a*b*c;
	}
	public static double pyramide(double a, double h) {
		return a*a*h/3;
	}
}
