import java.util.Scanner;

class Methoden {
	
	static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {

		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double r�ckgabe = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(r�ckgabe); 

		// zuZahlenderBetrag = ;

		//System.out.print("Zu zahlender Betrag: ");
		//System.out.printf("%.2f", zuZahlenderBetrag);
		//System.out.print("Euro\n");

		// Rechnung

		// Geldeinwurf
		// -----------
		
			// System.out.printf("format", ausgabe);
			
		// Fahrscheinausgabe
		// -----------------
		
		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		
		
	}

	public static double fahrkartenbestellungErfassen() {
		
		System.out.print("Ticketpreis (EURO): ");
		double ticketpreis = tastatur.nextDouble();
		if (ticketpreis<=0) {
			System.out.println("Der Ticketpreis muss �ber 0 Euro liegen und wird deswegen auf 1 Euro gesetzt!");
			ticketpreis = 1.00;
		}
		else {
			System.out.println("Der Ticketpreis betr�gt "+ ticketpreis);
		}
	
		System.out.print("Wie viele Tickets m�chten Sie erwerben?: ");
		int ticketAnzahl = tastatur.nextInt();
		
		if (ticketAnzahl>10 || ticketAnzahl<1 ) {
			System.out.println("Sie k�nnen nur 1 bis 10 Tickets erwerben!");
			System.out.println("Die Anzahl der Tickets wird deswegen auf eins zur�ckgesetzt.");
			ticketAnzahl = 1;
		}
		else {
			System.out.println("Sie m�chten "+ ticketAnzahl + " Tickets erwerben.");
		}
		
		double zuZahlenderBetrag = ticketAnzahl*ticketpreis;
		return zuZahlenderBetrag;
		
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag ) {
		
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze = 0.0;
		
		while (eingezahlterGesamtbetrag<zuZahlenderBetrag) {
		System.out.print("Noch zu zahlen: ");
		System.out.printf("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag);
		System.out.print("Euro\n");
		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		eingeworfeneM�nze = tastatur.nextDouble();
		eingezahlterGesamtbetrag += eingeworfeneM�nze; //eingezahlterGesamtbetrag=eingezahlterGesamtbetrag+eingeworfeneM�nze
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag; 
	}

public static void fahrkartenAusgeben() {
	System.out.println("\nFahrschein wird ausgegeben");
	for (int i = 0; i < 8; i++) {
		System.out.print("=");
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	System.out.println("\n\n");
}
public static void rueckgeldAusgeben(double r�ckgabebetrag)
{

	if (r�ckgabebetrag > 0.0) {
		System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
		System.out.println("wird in folgenden M�nzen ausgezahlt:");
	while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	{
		System.out.println("2 EURO");
		r�ckgabebetrag -= 2.0;
	}
	while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	{
		System.out.println("1 EURO");
		r�ckgabebetrag -= 1.0;
	}
	while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	{
		System.out.println("50 CENT");
		r�ckgabebetrag -= 0.5;
	}
	while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	{
		System.out.println("20 CENT");
		r�ckgabebetrag -= 0.2;
	}
	while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	{
		System.out.println("10 CENT");
		r�ckgabebetrag -= 0.1;
	}
	while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	{
		System.out.println("5 CENT");
		r�ckgabebetrag -= 0.05;
	}	
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
		}
	}
}
